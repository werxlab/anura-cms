# Anura CMS

is a Content Management System built on top of the latest "LTS" version of the [Symfony PHP Framework](https://symfony.com).

Anura (literally without tail in Ancient Greek). Amphibians composing the order Anura are commonly known as frogs or toads, which we all know are widely spread all over the globe. We hope to spread this CMS in a very similar way - all over the globe. 

If you want more info about **Anura**, see this [WIKIPEDIA page](https://en.wikipedia.org/wiki/Frog).

## Features

## Documetation

## Bugs, Issues, Support....

Issues, Bug Reports, Suuport, Suggestions and more within our [ISSUES area](../../issues).

## Roadmap

Follow our development plans - see [the Milestone area](../../milestones).

## License

BSD 2-Clause License. See [LICENSE](LICENSE) for details.

## Contributing

## Code Of Conduct

